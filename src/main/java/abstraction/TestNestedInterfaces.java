package abstraction;

public class TestNestedInterfaces {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TestNestedInterface obj = new TestNestedInterface();
		obj.msg();

		TestNestedInterface.InnerInterface objI = new TestNestedInterface.InnerInterface() {
			@Override
			public void show() {
				// TODO Auto-generated method stub
				System.out.println("The message from the outer of inner interface");
			}
		};
		objI.show();

	}

}

interface ShowableTest {
	void show();

	interface Message {
		void msg();
	}

}

class TestNestedInterface implements ShowableTest.Message {

	interface InnerInterface {
		void show();
	}

	@Override
	public void msg() {
		// TODO Auto-generated method stub
		System.out.println("This is the message");

		InnerInterface aobj = new InnerInterface() {
			@Override
			public void show() {
				// TODO Auto-generated method stub
				System.out.println("The message from the inner interface");
			}
		};
		aobj.show();

	}

}