package abstraction;

public class TestInterfaceDefault {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Drawable d = new RectangleI();
		d.draw();
		Drawable.msg();
		
	}

}

interface Drawable {
	
   void draw();
	
	public static void msg() {
		System.out.println("Static message is displayed");
	}
}

interface Square {
	void draw();

	default void msg() {
		System.out.println("Default method from square");
	}
}

class RectangleI implements Drawable,Square {
	@Override
	public void draw() {
		// TODO Auto-generated method stub
		System.out.println("Drawing the rectangle");

	}
}