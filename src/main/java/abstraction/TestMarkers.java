package abstraction;

public class TestMarkers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TestClass obj1 = new TestClass();
		obj1.a = 3;

		TestClass obj2 = obj1.cloneMe();
		obj2.a = 2;

	}

}

class TestClass {

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public TestClass cloneMe() {
		try {

			return (TestClass) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO: handle exception
			System.out.println("Clone is not supported!");
			return null;
		}
	}

	public int a;

}