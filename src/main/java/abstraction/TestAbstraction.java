package abstraction;

public class TestAbstraction {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bike obj = new Honda();
		obj.run();
		obj.changeGear(new Gear());

	}

}

abstract class Bike {

	Gear gear;
	protected String state = "Parked";

	public Bike() {
		// TODO Auto-generated constructor stub
		System.out.println("Bike is created");

	}

	abstract void run();

	void changeGear(Gear gear) {
		this.gear = gear;
	}

}

class Honda extends Bike {
	@Override
	void run() {
		// TODO Auto-generated method stub
		this.state = "Is running";

	}
}

class Gear {

}