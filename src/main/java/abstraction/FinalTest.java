package abstraction;

class A {

	public int number2;
	private int number;

	A(int number) {
		this.number = number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getNumber() {
		return this.number;
	}

}

public class FinalTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		final A instance = new A(5);
		A instance2 = new A(2);

		System.out.println(instance.getNumber());
		instance.setNumber(7);
		System.out.println(instance.getNumber());
	}

}
