package abstraction;

public class TestAnonymousClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestAnonymousInner.test();
		TestAnonymInterface.test();

	}

}

abstract class Person {
	abstract void eat();
}

interface Kid {
	void eat();
}

class TestAnonymousInner {

	public static void test() {

		Person p = new Person() {

			@Override
			void eat() {
				// TODO Auto-generated method stub
				System.out.println("I'm eating!");
			}
		};

		p.eat();
	}
}

class TestAnonymInterface {
	public static void test() {
		Kid kid = new Kid() {

			@Override
			public void eat() {
				// TODO Auto-generated method stub
				System.out.println("The kid is eating!");
			}
		};

		kid.eat();
	}

}
