package abstraction;

public class TestFactory {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		FactoryClass obj = (FactoryClass) FactoryClass.getInstance(2);

	}

}

class FactoryClass implements Factory {

	private FactoryClass() {

	}

	public static Factory getInstance(int position) {
		switch (position) {
		case 1:
			return new BoeingFactory();
		case 2:
			return new FactoryClass();
		default:
			return null;
		}

	}

}

class BoeingFactory implements Factory {

}

interface Factory {
}
