package abstraction;

public class SurfacePro implements Computer {

	private State state;

	@Override
	public void powerOn() {
		// TODO Auto-generated method stub
		this.state.setState("On");

	}

}

class State {
	private String state = "Off";

	public void setState(String state) {
		if (this.state == state)
			System.out.println("Computer is already " + state);
		else
			this.state = state;
	}

	public String getState() {
		return this.state;
	}

}
