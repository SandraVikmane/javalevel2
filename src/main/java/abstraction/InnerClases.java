package abstraction;

public class InnerClases {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TestMemberOuter1.test();

	}

}

class TestMemberOuter1 {

	private int data = 30;

	class Inner {

		private int value;

		public Inner() {
			// TODO Auto-generated constructor stub
			this.value = TestMemberOuter1.this.data;
		}

		void msg() {
			System.out.println("Value is " + this.value);
		}

	}

	private void changeValue(int data) {
		this.data = data;
	}

	public static void test() {
		TestMemberOuter1 obj = new TestMemberOuter1();
		TestMemberOuter1.Inner inObj = obj.new Inner();
		inObj.msg();

		obj.changeValue(54);
		inObj = obj.new Inner();
		inObj.msg();

	}

}

class OuterClassTest {

	private int a;

	class InnerClassTest {

		private int a = OuterClassTest.this.a;

		public InnerClassTest(int a) {
			// TODO Auto-generated constructor stub
			OuterClassTest.this.a = a;
			this.a = a;

		}

		public void setA(int a) {
			this.a = a;
		}

	}

	public OuterClassTest(int a) {
		// TODO Auto-generated constructor stub
		this.a = a;
		InnerClassTest obj = new InnerClassTest(2);
		obj.a = 3;

	}

}
