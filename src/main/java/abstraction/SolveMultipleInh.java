package abstraction;

interface MainA {

	default void mainVoid() {
		System.out.println("Default method from interface MainA");
	}

}

interface MainB {

	default void mainVoid() {
		System.out.println("Default method from interface MainB");
	}

}

public class SolveMultipleInh implements MainA, MainB {

	public static void main(String args[]) {
		SolveMultipleInh obj = new SolveMultipleInh();
		obj.mainVoid();
	}

	@Override
	public void mainVoid() {
		// TODO Auto-generated method stub
		MainA.super.mainVoid();
		MainB.super.mainVoid();
	}

}
