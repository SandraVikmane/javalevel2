package abstraction;

public class TestBlank {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank b = new SBI();
		System.out.println("Rate of Interest is:" + b.getRateOfInterest() + " %");

		b = new PNB();
		System.out.println("Rate of Interest is:" + b.getRateOfInterest() + " %");
	}

}

abstract class Bank {

	abstract int getRateOfInterest();

}

class SBI extends Bank {

	@Override
	int getRateOfInterest() {
		// TODO Auto-generated method stub
		return 7;
	}
}

class PNB extends Bank {
	@Override
	int getRateOfInterest() {
		// TODO Auto-generated method stub
		return 8;
	}
}