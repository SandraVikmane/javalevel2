package abstraction;

public class TestStaticClasses {

	public static void main(String args[]) {
		TestOuter.Inner obj = new TestOuter.Inner();

		TestOuter objOut = new TestOuter();

//		TestOuter.Inner2 obj2 = TestOuter.new Inner2();
		TestOuter.Inner2 obj2 = objOut.new Inner2();

		TestOuter.Inner.display();
	}

}

class TestOuter {
	static int data = 23;

	static class Inner {

		static class InnerInner {
		}

		void msg() {
			System.out.println("Data is " + TestOuter.data);
		}

		public static void display() {
			System.out.println("Static void is called");
		}
	}

	class Inner2 {
		void msg() {
			System.out.println("Data is " + TestOuter.data);
		}
	}

}
