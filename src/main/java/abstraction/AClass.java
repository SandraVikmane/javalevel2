package abstraction;

abstract class UtilClass {
	public static int a;
}

public abstract class AClass {

	public abstract void method();

	public void implementedMethod() {

	}

	public AClass() {
		// TODO Auto-generated constructor stub
	}

}

abstract class BClass extends AClass {
	public abstract void method2();

	@Override
	public void method() {
		// TODO Auto-generated method stub

	}

}

class extendedClass extends AClass {

	@Override
	public void method() {
		// TODO Auto-generated method stub
		System.out.println("Implememnted abstract method is called!");
		this.implementedMethod();

		AClass instance = new extendedClass();

	}

}
