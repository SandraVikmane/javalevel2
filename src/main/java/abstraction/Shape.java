package abstraction;

public abstract class Shape {
	abstract void draw();

	public static void main(String args[]) {
		Shape s = new Rectangle();
		s.draw();
	}

}

class Rectangle extends Shape {
	@Override
	void draw() {
		// TODO Auto-generated method stub
		System.out.println("Drawing the rectangle");
	}
}

class Circle extends Shape {

	@Override
	void draw() {
		// TODO Auto-generated method stub
		System.out.println("Drawing the circle");
	}

}
