package xml;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TestXML {

	public static void main(String args[]) {

		try {

			TestXML testxml = new TestXML();
			testxml.createXML();

		} catch (ParserConfigurationException e) {

		}

		String pathFile = ".src/main/java/xml/band.xml";
		String contentFile;
		try (Stream<String> lines = Files.lines(Paths.get(pathFile))) {

			// default StandardCharsets.UTF_8
			contentFile = lines.collect(Collectors.joining(System.lineSeparator()));

		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

	}

	private void createXML() throws ParserConfigurationException {

		try {
			Element element;

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document document = docBuilder.newDocument();

			Element bandElement = document.createElement("band");
			document.appendChild(bandElement);
			bandElement.setAttribute("bandName", "The Beatles");

			Element memberElement = document.createElement("member");
			memberElement.setAttribute("position", "bassist");

			Element bassistName = document.createElement("name");
			bassistName.setTextContent("Paul Mccartney");

			memberElement.appendChild(bassistName);
			bandElement.appendChild(memberElement);

			memberElement = document.createElement("member");
			memberElement.setAttribute("position", "rythm guitarist");

			Element name = document.createElement("name");
			name.setTextContent("John Lennon");

			memberElement.appendChild(name);
			bandElement.appendChild(memberElement);

			memberElement = document.createElement("member");
			memberElement.setAttribute("position", "drummer");

			name = document.createElement("name");
			name.setTextContent("Ringo Starr");

			memberElement.appendChild(name);
			bandElement.appendChild(memberElement);

			memberElement = document.createElement("member");
			memberElement.setAttribute("position", "guitarist");

			name = document.createElement("name");
			name.setTextContent("George Harrison");

			memberElement.appendChild(name);
			bandElement.appendChild(memberElement);

			DOMSource domSource = new DOMSource(document);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();

			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			transformer.transform(domSource, result);
			System.out.println(writer.toString());

			TestXML test = new TestXML();
			test.validateXML(writer.toString());

		} catch (TransformerException e) {
			// TODO: handle exception
		}

	}

	private void validateXML(String content) {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		try {
			Schema schema = factory.newSchema(new StreamSource(new File("./src/xml/Band.xsd")));

			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new StringReader(content)));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
