package jsonTest;

import java.util.List;

public class Glossary {
	private String title;
	private char division;
	private List<GlossEntry> glosseries;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public char getDivision() {
		return division;
	}
	public void setDivision(char division) {
		this.division = division;
	}
	public List<GlossEntry> getGlosseries() {
		return glosseries;
	}
	public void setGlosseries(List<GlossEntry> glosseries) {
		this.glosseries = glosseries;
	}
	

}
