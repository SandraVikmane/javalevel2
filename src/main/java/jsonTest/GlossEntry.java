package jsonTest;

public class GlossEntry {
	private String id;
	private String SortAs;
	private String glossTerm;
	private String acronym;
	private String abbrev;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSortAs() {
		return SortAs;
	}
	public void setSortAs(String sortAs) {
		SortAs = sortAs;
	}
	public String getGlossTerm() {
		return glossTerm;
	}
	public void setGlossTerm(String glossTerm) {
		this.glossTerm = glossTerm;
	}
	public String getAcronym() {
		return acronym;
	}
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
	public String getAbbrev() {
		return abbrev;
	}
	public void setAbbrev(String abbrev) {
		this.abbrev = abbrev;
	}
	

}
