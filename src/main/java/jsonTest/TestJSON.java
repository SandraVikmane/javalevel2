package jsonTest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.json.*;


public class TestJSON {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        String path = ".src/main/java/jsonTest/Glossary.txt";
		String content = TestJSON.getContent(path);
//		System.out.println(content);

		TestJSON.processFile(content);
		
		TestJSON.createJSON();

	}

	public static String getContent(String path) {
		try (Stream<String> lines = Files.lines(Paths.get(path))) {
			return lines.collect(Collectors.joining(System.lineSeparator()));

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}

	}

	public static void processFile(String content) {

		JSONObject fileObject = new JSONObject(content);

		Glossary glossary = new Glossary();

		JSONObject glossaryValue = fileObject.getJSONObject("glossary");
		String title = glossaryValue.getString("title");
		glossary.setTitle(title);

		JSONObject glossDiv = glossaryValue.getJSONObject("GlossDiv");
		char division = glossDiv.getString("title").charAt(0);
		glossary.setDivision(division);

		List<GlossEntry> glosseries = new ArrayList<GlossEntry>();
		glossary.setGlosseries(glosseries);

		JSONObject glossList = glossDiv.getJSONObject("GlossList");
		JSONArray glossItems = glossList.getJSONArray("GlossEntries");

		Iterator<Object> iterator = glossItems.iterator();

		while (iterator.hasNext()) {
			JSONObject item = (JSONObject) iterator.next();
			GlossEntry glossItem = new GlossEntry();

			glossItem.setId(item.getString("ID"));
			glossItem.setAbbrev(item.getString("Abbrev"));
			glossItem.setAcronym(item.getString("Acronym"));
			glossItem.setGlossTerm(item.getString("GlossTerm"));
			glossItem.setSortAs(item.getString("SortAs"));
			glosseries.add(glossItem);

		}

//		System.out.print("Dummy");

	}

	public static void createJSON() {

		String[] employees = { "Janis", "Peteris", "Maris", "Liga", "Mara" };
		JSONArray employeesArray = new JSONArray();

		for (int i = 0; i < employees.length; i++) {

			JSONObject employee = new JSONObject();
			employee.put("Name", employees[i]);
			employee.put("Department", "IT");

			employeesArray.put(employee);

		}

		JSONObject company = new JSONObject();
		
		company.put("Employess", employeesArray);
		company.put("title", "CompanyName");
		System.out.println(company.toString());

	}

}