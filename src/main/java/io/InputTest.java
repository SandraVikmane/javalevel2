package io;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class InputTest {

	public static void main(String[] args) {

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String line;
		while (line = input.readLine() != null && line.length() != 0) {
			System.out.println(line);
		System.err.println(line);
		}

	}

}
