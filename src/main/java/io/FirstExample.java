package io;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.h2.store.fs.FilePath;

public class FirstExample {

	public static void main(String[] args) throws Exception {

		//FileOutputStream file = new FileOutputStream ("C:\\Users\\PCMAN\\Documents\\JAVA_BOOTCAMP_1\\todo2.csv");
		//new file is created
		//PrintStream printStreamObject = new PrintStream (file);
		//printStreamObject.println("1;Surname;Name");
		//printStreamObject.println("1;Surname2;Name2");
		//printStreamObject.close();
		//file.close();
		//System.out.println("File is closed");
		

		
		File file2 = new File ("C:\\Users\\PCMAN\\Documents\\JAVA_BOOTCAMP_1\\todo3.xml");
		DateFormat df = new SimpleDateFormat ("dd.MM.yyy HH:mm:ss");
		System.out.println(df.format(file2.lastModified()));
		
		File file3 = new File ("C:\\Users\\PCMAN\\Documents\\JAVA_BOOTCAMP_1\\todo2.csv");
		try {
			BufferedReader reader = new BufferedReader (new FileReader (file3));
			String s;
			s = reader.readLine();
			while (s != null) {
				System.out.println(s);
				s = reader.readLine();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		File file4 = new File ("C:\\Users\\PCMAN\\Documents\\JAVA_BOOTCAMP_1\\todo2.csv");
		PrintWriter writer = new PrintWriter (new FileWriter (file4));
		writer.println("1;Surname;Name");
		writer.println("2;Surname2;Name2");
		writer.close();
		System.out.println("file finished");
	
		//System.out.println("Output");
		//System.err.println("Error");
		//throw new Exception ("Error");
		
		//char letter [] = new char [5];
		//for (int i = 0; i < 5; i++) {
		//	letter[i] = (char) System.in.read(); // use cast because returning type is integer
		//} // if we cast the hasty type, it returns something, but we need char or char apzīmējumnumuru
		
		
		//int letter = System.in.read();
		//System.out.println(letter);
		//System.out.println(letter);.toString());

		
		//FileOutputStream file = new FileOutputStream ("C:\\Users\\PCMAN\\Documents\\JAVA_BOOTCAMP_1\\todo.txt");
		//String text = "Text is here using bytes";
		
		//for (int i = 0; i < text.length(); i++) {
		//	file.write(text.charAt(i));
		//}
		
		//file.write(text.getBytes());
		//file.close();
		//System.out.println("Succesfully written"); // to write in the file!
		
		//Reader reader = new FileReader("C:\\Users\\PCMAN\\Documents\\JAVA_BOOTCAMP_1\\todo.txt");
		//int i = 0;
		// while ((i = reader.read()) != -1) // to read from the file!
		//	 System.out.print((char) i);
		// reader.close(); 
			 
	}

}
