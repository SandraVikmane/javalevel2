package jtm.activity09;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;


/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders                                             D
 * - add(Order item)            — add passed order to the Orders    
 * - List<Order> getItemsList() — List of all customer orders                                      D
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)   D
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String                                  D
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in List.sort() method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */

public class Orders implements Iterable<Order> { // this maybe used as iteratros or as the collections of orders.

	public static void main(String[] args) {
		
	}
	/*- !! needs to have a collection of orders + the current index of the collection (to know the position we are)
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */

	 // need to controll the access of the list

	List<Order> orderList = new ArrayList<Order>();
	private int currentCount;

	public boolean hasNext() {
		if (this.orderList.size() > 0 && currentCount < this.orderList.size()) {
			return true;
		} else
			return false;
	}

	public Order next() {
		if (this.hasNext()) {
			this.currentCount++;
			return this.orderList.get(this.currentCount);
		} else
			throw new NoSuchElementException();
	}

	public Orders() {
		this.orderList = new ArrayList<Order>();
		this.currentCount = -1;
	}

	public void add(Order item) {
		this.orderList.add(item);
	}

	public List<Order> getItemsList() {
		return this.orderList;
	}

	public Set<Order> getItemsSet() {
		
		Set<Order> orderSet = new TreeSet<Order>();
		Order previous = null;
		this.sort();
		
		for (Order order : orderSet) {

			if (previous == null) {
				previous = order;
				continue;
			}

			if (order.name.equals(previous.name)) {
				previous.count += order.count;
				previous.customer += ", " + order.customer;
			} else {
				orderSet.add(previous);
				previous = order;
			}
		}
		if (previous != null)
			orderSet.add(previous);
		return orderSet;
	}

	@Override
	public String toString() {
		return orderList.toString();
	}

	public void remove() {
		if (this.currentCount >= 0) {
			orderList.remove(currentCount);
			currentCount--;
		} else
			throw new IllegalStateException();
	}

	public void sort() {
		Collections.sort(this.orderList);
	}

	@Override
	public Iterator<Order> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

}
