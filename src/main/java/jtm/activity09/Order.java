package jtm.activity09;

import java.util.ArrayList;
import java.util.List;

/*- TODO #1
 * Implement Comparable interface with Order class
 * Hint! Use generic type of comparable items in form: Comparable<Order>
 * 
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) — constructor of the Order           D
 * - public int compareTo(Order order) — comparison implementation according to logic described below  D
 * - public boolean equals(Object object) — check equality of orders                                   D
 * - public int hashCode() — to be able to handle it in some hash... collection                        D
 * - public String toString() — string in following form: "ItemName: OrdererName: Count"               D
 * !!!! call the method hashcode() from this "ItemName: OrdererName: Count" string.                 
 * 
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 * 
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 * 
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 * 
 */

public class Order implements Comparable<Order> {
	String customer; // Name of the customer
	String name; // Name of the requested item
	int count; // Count of the requested items

	public Order(String orderer, String itemName, Integer count) {
		this.customer = orderer;
		this.name = itemName;
		this.count = count;
	}
	
	Order order1 = new Order ("Sandra", "Abols", 2);
	Order order2 = new Order ("Liene", "Abols", 3);
	Order order3 = new Order ("Sandra", "Banans", 1);

	@Override
	public int compareTo(Order o) {

		if (this.name.compareTo(o.name) < 0)
			return -1;
		else if ((this.name.compareTo(o.name) > 0))
			return 1;
		else if ((this.name.compareTo(o.name) == 0)) {

			if (this.customer.compareTo(o.customer) < 0)
				return -1;
			else if ((this.customer.compareTo(o.customer) > 0))
				return 1;
			else if (this.customer.compareTo(o.customer) == 0) {

				if (this.count < o.count)
					return -1;
				if (this.count > o.count)
					return 1;
				if (this.count == o.count)
					return 0;
			}
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Order order = (Order) obj;
		return count == order.count && (name.equals(order.name) && customer.equals(order.customer));
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public String toString() {
		return "ItemName: " + this.name + " OrdererName: " + this.customer + " Count: " + this.count;
	}

	public Object getName() {
		// TODO Auto-generated method stub
		return this.name;
	}

}
