package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Amphibia extends Vehicle {
	private byte numberOfSails;
	private int wheels;
	// private Vehicle vehicle;
	//private Ship ship;

	/*
	 * public void initilize(String string) { this.ship = new Ship("ddd", (byte) 9);
	 * this.vehicle = new Vehicle("ggg", 5f, 6, 7); }
	 */

	public Amphibia(String id, float consumption, int tankSize,  byte numberOfSails, int wheels) {
		super(id, consumption, tankSize, wheels);
		this.numberOfSails = numberOfSails;
		this.wheels = wheels;
	}

	/*public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		this.numberOfSails = sails;
		this.wheels = wheels;
	}*/

	@Override
	public String move(Road road) {
		if (road instanceof WaterRoad) {
			WaterRoad waterRoad = (WaterRoad) road;
			return getId() + " " + getType() + " is sailing on " + waterRoad + " with "
					+ this.numberOfSails + " sails";
		}
		else {
			String s = super.move(road);
			boolean canNot = s.contains("Cannot");
			if (canNot == false) {
				return getId() + " " + super.getType() + " is driving on " + road.toString() + " with " + this.wheels
						+ " wheels";
			} else 
			return "Cannot drive on " + road.toString();
		}
	}
	
	
	/*public String move(Road road) {
		if (road instanceof WaterRoad) {
			WaterRoad waterRoad = (WaterRoad) road;
			return Ship.move(road, this);
		} else 
			return super.move(road).replace("Vehicle", "Amphibia");
	}*/


	/*
	 * public String move(Road road) { if (road instanceof WaterRoad) { return
	 * getId() + getType() + " is sailing on " + road.toString();
	 * 
	 * } else { return getId() + getType() + " is moving on " + road.toString(); } }
	 */

	/*
	 * public void move(String media) { if (media == "ground")
	 * System.out.println(getId() + " " + getType() + " is moving on " +
	 * this.getFrom() + " — " + this.getTo() + " " + this.getDistance() +
	 * " km with " + this.wheels + " wheels"); if (media == "water")
	 * System.out.println(getId() + " " + getType() + " is sailing on " +
	 * this.getFrom() + " — " + this.getTo() + " " + this.getDistance() +
	 * " km with " + this.numberOfSails + " sails"); }
	 */

	public static void main(String[] args) {
		Amphibia amphibia = new Amphibia("mmm", 5.5678f, 6, (byte) 7, 8);
		amphibia.setFrom("Ventspils");
		amphibia.setTo("Gotlande");
		amphibia.setDistance(1145);
		amphibia.move(new WaterRoad("Ventspils", "Gotlande", 456));
		amphibia.setFrom("Riga");
		amphibia.setTo("Ogre");
		amphibia.setDistance(37);
		//amphibia.move(amphibia);
		System.out.println(amphibia.move(new WaterRoad("Ventspils", "Gotlande", 160)));
		System.out.println(amphibia.move(new Road("Ventspils", "Riga", 186)));
		System.out.println(amphibia.getConsumption());
		System.out.println(amphibia.toString());
	}

}
