package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

class Vehicle extends Transport {
	protected int numberOfWheels;

	public Vehicle(String id, float consumption, int tankSize, int wheels) {
		super(id, consumption, tankSize);
		this.numberOfWheels = wheels;

		// TODO Auto-generated constructor stub
	}

	@Override
	public String move(Road road) {
		String whatToReturn = "";
		if (road instanceof WaterRoad) {
			return "Cannot drive on " + road.toString();
		} else {
			whatToReturn = super.move(road);
			boolean is = whatToReturn.contains("is");
			if (is == true) {
				whatToReturn = whatToReturn.replace("moving", "driving") + " with " + this.numberOfWheels + " wheels";
			} else
				return "Cannot drive on " + road.toString();
		}
		return whatToReturn;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Vehicle vehicle = new Vehicle("yyy", 1f, 2, 3);

	}

}
