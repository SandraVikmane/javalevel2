package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Ship extends Transport {

	protected byte numberOfSails;

	public Ship(String id, byte sails) {
		super(id, 0, 0);
		this.numberOfSails = sails;
		// TODO Auto-generated constructor stub
	}

	/*
	 * public Ship(String id, float consumption, int tankSize) { super(id,
	 * consumption, tankSize); // TODO Auto-generated constructor stub }
	 */

	@Override
	public String move(Road road) {
		if (!(road instanceof WaterRoad))
			return "Cannot sail on " + road.toString();
		else
			return getId() + " " + getType() + " is sailing on " + road.toString() + " with " + this.numberOfSails + " sails";
	}

	public static void main(String[] args) {
		Ship ship = new Ship("sss", (byte) 8);
		System.out.println(ship.move(new WaterRoad("Ventspils", "Gotlande", 456)));

	}

}