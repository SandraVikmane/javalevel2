package jtm.activity06;

public class Martian implements Humanoid, Alien, Cloneable {

	private int weight;
	private Object stomach;

	public Martian() {
		weight = Alien.BirthWeight;
	}

	public Martian(int weight) {
		this.weight = weight;
	}

	@Override
	public void eat(Object item) {
		// TODO Auto-generated method stub
		if (!(stomach == null)) {
			return;
		}
		this.stomach = item;

		if (item instanceof Human) {
			this.weight += ((Human) item).getWeight();
			((Humanoid) item).killHimself();
		} else if (item instanceof Martian) {
			this.weight += ((Martian) item).getWeight();
		} else if (item instanceof Integer)

		{
			this.weight += ((Integer) item);
		}
	}

	@Override
	public void eat(Integer food) {

		if (!(stomach == null)) {
			return;
		}
		stomach = food;
		weight = weight + (int) stomach;
	}

	@Override
	public Object vomit() {

		if (stomach == null)
			return null;

		if (this.stomach instanceof Human)
			this.weight -= ((Humanoid) this.stomach).getWeight();
		else if (this.stomach instanceof Martian)
			this.weight -= ((Alien) this.stomach).getWeight();
		else if (this.stomach instanceof Integer)
			weight -= ((Integer) this.stomach);

		Object currentStomach = this.stomach;
		this.stomach = null;
		return currentStomach;
	}

	// if the stomach is full you do not eat again
	//
	//

	@Override
	public String isAlive() {
		// TODO Auto-generated method stub
		return "I AM IMMORTAL!";
	}

	@Override
	public String killHimself() {
		// TODO Auto-generated method stub
		return isAlive();
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return weight;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return clone(this);
	}

	private Object clone(Object current) {
		if (current == null)
			return null;

		if (current instanceof Integer)
			return current;

		if (current instanceof Alien) {
			Martian currentMartian = (Martian) current;
			Martian newMartian = new Martian(currentMartian.getWeight());
			newMartian.setStomach(this.clone(currentMartian.getStomach()));
			return newMartian;
		}
		if (current instanceof Human) {
			Human newHuman = new Human(((Human) current).getWeight());
			newHuman.setStomach(((Human) current).getStomach());
			return newHuman;
		}
		return new Object(); // Dummy
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getClass().getSimpleName() + ": " + this.weight + " [" + this.stomach + "]";
	}

	public void setStomach(Object stomach) {
		this.stomach = stomach;
	}

	public Object getStomach() {
		return this.stomach;
	}
}