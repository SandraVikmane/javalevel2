package jtm.activity06;

public class Human implements Humanoid {

	int stomach;
	boolean isAlive;
	int birthWeight;
	int weight;

	public Human() {
		stomach = 0;
		isAlive = true;
		birthWeight = BirthWeight;
		weight = birthWeight + stomach;
	}
	
	public Human (int weight) {
		this.weight = weight;
	}
	

	@Override
	public void eat(Integer food) {
		// TODO Auto-generated method stub
		if (stomach == 0) 
			//weight = food;
			stomach = (int) food;
			//birthWeight += weight;
		weight = birthWeight + stomach;
	}

	@Override
	public Integer vomit() {
		// TODO Auto-generated method stub
		weight -= stomach;
		int vomit = stomach;
		stomach = 0;
		return vomit;
	}

	// this.weight -= this.eatenItem; this.weight ir food
	// this.currentEaten = this.eatenItem;
	// this.eatenItem = 0;
	// return currentEaten;

	@Override
	public String isAlive() {
		// TODO Auto-generated method stub
		if (isAlive)
			return "Alive";
		else
			return "Dead";
	}

	@Override
	public String killHimself() {
		// TODO Auto-generated method stub
		isAlive = false;
		return isAlive();
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return birthWeight + stomach;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getClass().getSimpleName() + ": " + this.weight + " [" + this.stomach + "]";
	}
	
	public void setStomach (int stomach) {
		this.stomach = stomach;
	}
	
	public int getStomach() {
		return stomach;
	}

}
