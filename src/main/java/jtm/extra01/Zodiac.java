package jtm.extra01;

public class Zodiac {

	/**
	 * Determine the sign of the zodiac, use day and month.
	 * 
	 * @param day
	 * @param month
	 * @return zodiac
	 */
	public static String getZodiac(int day, int month) {

		String zodiac = null;
		// TODO #1: Implement method which return zodiac sign names
		// As method parameter - day and month;
		// Look at wikipedia:
		// https://en.wikipedia.org/wiki/Zodiac#Table_of_dates
		// Tropical zodiac, to get appropriate date ranges for signs

		Dates date = new Dates(day, month);

		if (!date.checkInput())
			return "Error";

		if ((month == 1 && day > 20) || (month == 2 && day < 20))
			zodiac = "Aquarius";

		else if ((month == 2 && day > 19) || (month == 3 && day < 21))
			zodiac = "Pisces";

		else if ((month == 3 && day > 20) || (month == 4 && day < 21))
			zodiac = "Aries";

		else if ((month == 4 && day > 20) || (month == 5 && day < 22))
			zodiac = "Taurus";

		else if ((month == 5 && day > 21) || (month == 6 && day < 22))
			zodiac = "Gemini";

		else if ((month == 6 && day > 21) || (month == 7 && day < 23))
			zodiac = "Cancer";

		else if ((month == 7 && day > 22) || (month == 8 && day < 23))
			zodiac = "Leo";

		else if ((month == 8 && day > 22) || (month == 9 && day < 24))
			zodiac = "Virgo";

		else if ((month == 9 && day > 23) || (month == 10 && day < 24))
			zodiac = "Libra";

		else if ((month == 10 && day > 23) || (month == 11 && day < 23))
			zodiac = "Scorpio";

		else if ((month == 11 && day > 22) || (month == 12 && day < 22))
			zodiac = "Sagittarius";

		else if ((month == 12 && day > 21) || (month == 1 && day < 21))
			zodiac = "Capricorn";

		return zodiac;

	}

	public static void main(String[] args) {
		// HINT: you can use main method to test your getZodiac method with
		// different parameters
		System.out.println(getZodiac(37, 34));
	}

}

class Dates {

	private int day;
	private int month;
	final static String DAY = "Day";
	final static String MONTH = "Month";

	public Dates(int day, int month) {
		// TODO Auto-generated constructor stub
		this.day = day;
		this.month = month;
	}

	public boolean checkInput() {

		if (!this.checkMonth())
			return false;

		if (!this.checkDay())
			return false;

		switch (this.month) {
		case 1:
			if (!this.checkDay(31))
				return false;
		case 2:
			if (!this.checkDay(29))
				return false;
		case 3:
			if (!this.checkDay(31))
				return false;
		case 4:
			if (!this.checkDay(30))
				return false;
		case 5:
			if (!this.checkDay(31))
				return false;
		case 6:
			if (!this.checkDay(30))
				return false;
		case 7:
			if (!this.checkDay(31))
				return false;
		case 8:
			if (!this.checkDay(31))
				return false;
		case 9:
			if (!this.checkDay(30))
				return false;
		case 10:
			if (!this.checkDay(31))
				return false;
		case 11:
			if (!this.checkDay(30))
				return false;
		case 12:
			if (!this.checkDay(31))
				return false;
		}

		return true;
	}

	private void displayError(String daymonth) {
		System.out.println(daymonth + " is entered incorreclty!");
	}

	private boolean checkDay() {
		if (this.day < 1 || this.day > 31) {
			this.displayError(Dates.DAY);
			return false;
		}
		return true;
	}

	private boolean checkMonth() {
		if (this.month < 1 || this.month > 12) {
			this.displayError(Dates.MONTH);
			return false;
		}
		return true;
	}

	private boolean checkDay(int max) {
		if (this.day > max) {
			this.displayError(Dates.DAY);
			return false;
		}
		return true;
	}

}
