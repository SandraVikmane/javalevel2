package jtm.extra01;

public class GetOne {
	
	public static void main (String [] args) {
		GetOne getOne = new GetOne();
		
		System.out.println(getOne.iterations(6));
		System.out.println(getOne.theMostComplexNo(6));
	}

	public int iterations(int number) {
		// TODO #1: Implement method which processes the passed positive number
		// value until it's
		// reduced to 1.
		// If the number is even then divide it by 2. If it is odd then multiply
		// it by 3 and add 1. Count how many iterations
		// it takes to do this calculation and return that count. For example:
		// passed number is 6. Path to completion would be:
		// 6->3->10->5->16->8->4->2->1. Iteration count=8.
		// HINT: Use while loop.
		int iterationCount = 0;

		if (number < 1) {
			System.out.println("Number is not positive!");
			return 0;
		}
		while (number > 1) {
			if (number % 2 == 0) {
				number = number / 2;
				iterationCount++;
			} else {
				number = (number * 3) + 1;
				iterationCount++;
			}
		}
		return iterationCount;
	}

	/*
	 * int maxNumber = 5; int number1 = maxNumber; int number3 = 0; int
	 * iterationCount1 = 0; int[] arr = new int[maxNumber]; int index = 0;
	 * 
	 * for (int i = 1; i <= number1; i++) { while (number1 != 1) { if (number1 % 2
	 * == 0) { number3 = number1 / 2; // System.out.print(number3 + " "); number1 =
	 * number3; iterationCount1++; } else { number3 = (number1 * 3) + 1; //
	 * System.out.print(number3 + " "); number1 = number3; iterationCount1++; } }
	 * number1 = maxNumber; number1--; arr[index] = iterationCount1; iterationCount1
	 * = 0; index++; } for (int j = 0; j < arr.length; j++) {
	 * System.out.print(arr[j] + " "); return iterationCount1; } }
	 */

	public int theMostComplexNo(int maxNumber) {
		// TODO #2: Calculate how many iterations each number from 1 to
		// maxNumber (including) to get value till 1.
		// Return the number, which takes most iterations to do that.
		// E.g. if 3 is passed, then calculate iteration steps for 1, 2 and 3.
		// And return 3, because it has the biggest count of iterations.
		// (If count of iterations is the same for several numbers, return
		// smallest number).
		int longestPath = 0;
		int holder = 0;
		int maxSteps = -1;
		for (int i = 1; i <= maxNumber; i++) {
			holder = this.iterations(i);
			if (holder > maxSteps) {
				maxSteps = holder;
				longestPath = i;
			}
		}
		
		return longestPath;
	}

}