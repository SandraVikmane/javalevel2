package jtm.activity13;

import java.sql.Connection;
import java.util.List;
import jtm.TestUtils;
import jtm.testSuite.JTMTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseTest extends JTMTest {
	private static boolean dirtyDatabase = true;
	private static Connection conn;
	private static TeacherManager manager;
	private static String fnameToDelete = "To be";
	private static String lnameToDelete = "Deleted";
	private static int idToDelete = 5;

	@Before
	public void test02TeacherFields() {
		int id = TestUtils.randomInt(10, 20);
		String firstName = TestUtils.randomName();
		String lastName = TestUtils.randomName();
		Teacher teacher = new Teacher(id, firstName, lastName);
		Assert.assertEquals("Wrong value for teacher's id. Check constructor and getters.", (long) id,
				(long) teacher.getId());
		Assert.assertEquals("Wrong value for teacher's first name. Check constructor and getters.", firstName,
				teacher.getFirstName());
		Assert.assertEquals("Wrong value for teacher's last name. Check constructor and getters.", lastName,
				teacher.getLastName());
	}

	@Test
	public void test01SetUp() {
		try {
//			TestUtils.checkMySQL();
			String var10000 = "";
			String var10001 = "root";
//			Assert.assertTrue("0\n", "root".equals(TestUtils.executeCmd("")));
			String var10002 = "";
//			if ("root".equals(TestUtils.executeCmd(""))) {
//				logger.warn("There was no database 'database_activity' when test was started;");
//			}

			resetDatabase();
			logger.info("Database was dropped and recreated");
			String var10003 = "Database 'database_activity' is not created.";
//			Assert.assertTrue("0\n", TestUtils.user.equals(TestUtils.executeCmd(TestUtils.password)));
			manager = new TeacherManager();
			conn = manager.conn;
			Assert.assertNotEquals("TeacherManager connection is not initialized", (Object) null, conn);
			logger.debug("Connection successfully established!");
			logger.info("OK");
		} catch (Exception var2) {
			TestUtils.handleErrorAndFail(var2);
		}

	}

	@Test
	public void test03FindTeacherByID() {
		try {
			manager = new TeacherManager();
			Teacher result = manager.findTeacher(1);
			this.checkTeacher(result, "Mara", "Ett", 1L);
			result = manager.findTeacher(2);
			this.checkTeacher(result, "Sara", "Du", 2L);
			result = manager.findTeacher(-128);
			this.checkTeacher(result, (String) null, (String) null, 0L);
			logger.info("OK");
		} catch (Exception var2) {
			TestUtils.handleErrorAndFail(var2);
		}

	}

	@Test
	public void test04FindTeachersByNames() {
		try {
			List<Teacher> results = manager.findTeacher("RA", "E");
			Assert.assertEquals("findTeacher() error.", 2L, (long) results.size());
			this.checkTeachers(results, "Mara", "Ett", 1L, 0);
			this.checkTeachers(results, "Lara", "Tre", 3L, 1);
			results = manager.findTeacher("ARA", "DU");
			Assert.assertEquals("findTeacher() result size error.", 1L, (long) results.size());
			this.checkTeachers(results, "Sara", "Du", 2L, 0);
			results = manager.findTeacher("This teacher", "should NOT be found");
			Assert.assertEquals("findTeacher() error for negative search.", 0L, (long) results.size());
			logger.info("OK");
		} catch (Exception var2) {
			TestUtils.handleErrorAndFail(var2);
		}

	}

	@Test
	public void test05InsertTeacher() {
		try {
			String fname = "testF";
			String lname = "testL";
			boolean result = manager.insertTeacher(fname, lname);
			Assert.assertTrue(result);
			List<Teacher> results = manager.findTeacher(fname, lname);
			Assert.assertEquals("findTeacher() result size error.", 1L, (long) results.size());
			this.checkTeachers(results, fname, lname);
			logger.info("OK");
		} catch (Exception var5) {
			TestUtils.handleErrorAndFail(var5);
		}

	}

	@Test
	public void test06InsertTeacherAll() {
		try {
			String fname = "Insert";
			String lname = "All";
			int id = 5;
			Teacher teacher = new Teacher(id, fname, lname);
			boolean result = manager.insertTeacher(teacher);
			Assert.assertTrue(result);
			List<Teacher> results = manager.findTeacher(fname, lname);
			Assert.assertEquals("insertTeacherAll() results size error.", 1L, (long) results.size());
			this.checkTeachers(results, fname, lname, (long) id, 0);
			result = manager.insertTeacher(teacher);
			Assert.assertFalse(result);
			results = manager.findTeacher(fname, lname);
			Assert.assertEquals("insertTeacherAll() results size error.", 1L, (long) results.size());
			this.checkTeachers(results, fname, lname, (long) id, 0);
			fname = "Insert1";
			lname = "All1";
			Teacher teacher1 = new Teacher(5, fname, lname);
			result = manager.insertTeacher(teacher1);
			Assert.assertFalse(result);
			results = manager.findTeacher(fname, lname);
			Assert.assertEquals("insertTeacherAll() results size error.", 0L, (long) results.size());
			logger.info("OK");
		} catch (Exception var8) {
			TestUtils.handleErrorAndFail(var8);
		}

	}

	@Test
	public void test07UpdateTeacher() {
		try {
			Teacher teacher = new Teacher(5, fnameToDelete, lnameToDelete);
			boolean result = manager.updateTeacher(teacher);
			Assert.assertTrue(result);
			List<Teacher> results = manager.findTeacher(fnameToDelete, lnameToDelete);
			Assert.assertEquals("updateTeacher() results size error.", 1L, (long) results.size());
			this.checkTeachers(results, fnameToDelete, lnameToDelete, (long) idToDelete, 0);
			String fname = "This teacher";
			String lname = "Should not be updated";
			teacher = new Teacher(123, fname, lname);
			result = manager.updateTeacher(teacher);
			Assert.assertFalse(result);
			results = manager.findTeacher(fname, lname);
			Assert.assertEquals("updateTeacher() results size error.", 0L, (long) results.size());
			logger.info("OK");
		} catch (Exception var6) {
			TestUtils.handleErrorAndFail(var6);
		}

	}

	@Test
	public void test08DeleteTeacher() {
		try {
			boolean result = manager.deleteTeacher(idToDelete);
			Assert.assertTrue(result);
			List<Teacher> results = manager.findTeacher(fnameToDelete, lnameToDelete);
			Assert.assertEquals("deleteTeacher() results size error.", 0L, (long) results.size());
			result = manager.deleteTeacher(idToDelete);
			Assert.assertFalse(result);
			logger.info("OK");
		} catch (Exception var3) {
			TestUtils.handleErrorAndFail(var3);
		}

	}

	@Test
	public void test09TransactionCommit() {
		String name1 = TestUtils.getDate();
		String name2 = TestUtils.getDate();
		logger.debug(name1);

		try {
			TeacherManager manager1 = new TeacherManager();
			TeacherManager manager2 = new TeacherManager();
			manager2.conn.setAutoCommit(true);
			Assert.assertTrue(!manager1.equals(manager2));
			manager1.insertTeacher(name1, name1);
			List<Teacher> teachers = manager2.findTeacher(name1, name1);
			Assert.assertNotNull(
					"List of teachers for commited transaction test is empty. Check that transaction is commited, when teacher is added.",
					teachers);
			this.checkTeachers(teachers, name1, name1, (Long) null, (Integer) null,
					"Check that transaction is commited, when teacher is added.");
			int id = ((Teacher) teachers.get(0)).getId();
			name1 = TestUtils.randomLatinName();
			logger.debug(name1);
			manager1.updateTeacher(new Teacher(id, name2, name2));
			Teacher findTeacher = manager2.findTeacher(id);
			this.checkTeacher(findTeacher, name2, name2, (long) id,
					"Check that transaction is commited, when teacher is updated.");
			manager1.deleteTeacher(id);
			findTeacher = manager2.findTeacher(id);
			this.checkTeacher(findTeacher, (String) null, (String) null, 0L,
					"Check that transaction is commited, when teacher is deleted.");
			manager1.closeConnecion();
			manager2.closeConnecion();
			logger.info("OK");
		} catch (Exception var8) {
			TestUtils.handleErrorAndFail(var8);
		}

	}

	@Test
	public void test10InjectionTests() {
		Assert.assertNotNull("TeacherManager is not prepared", manager);

		try {
			boolean result = manager.insertTeacher("A;", ";drop database database_activity;--");
			Assert.assertTrue("SQL injection was probably executed", result);
			List<Teacher> teachers = manager.findTeacher("A;", ";drop database database_activity;--");
			this.checkTeachers(teachers, "A;", ";drop database database_activity;--");
			int id = ((Teacher) teachers.get(0)).getId();
			manager.deleteTeacher(id);
			logger.info("OK");
		} catch (Exception var4) {
			TestUtils.handleErrorAndFail(var4);
		}

	}

	@Test
	public void test11closeConnection() {
		try {
			Assert.assertNotNull("Connection is not prepared", manager.conn);
			manager.closeConnecion();
			Assert.assertNull("Connection is not set to null when closed.", manager.conn);
		} catch (Exception var2) {
			TestUtils.handleErrorAndFail(var2);
		}

	}

	public static synchronized void resetDatabase() {
		if (dirtyDatabase) {
			try {
				String workspace = System.getProperty("user.dir");
				String var10000 = TestUtils.user;
				String var10001 = TestUtils.password;
				TestUtils.executeCmd(workspace);
				logger.info("Database dump restored");
				dirtyDatabase = false;
			} catch (Exception var1) {
				TestUtils.handleErrorAndFail(var1);
			}
		} else {
			logger.info("Database is clean");
		}

	}

	private void checkTeachers(List<Teacher> results, String firstname, String lastname) {
		this.checkTeachers(results, firstname, lastname, (Long) null, (Integer) null, (String) null);
	}

	private void checkTeachers(List<Teacher> results, String firstname, String lastname, Long id, Integer recordno) {
		this.checkTeachers(results, firstname, lastname, id, recordno, (String) null);
	}

	private void checkTeachers(List<Teacher> results, String firstname, String lastname, Long id, Integer recordno,
			String additionalMessage) {
		if (recordno == null) {
			recordno = 0;
		}

		if (additionalMessage == null) {
			additionalMessage = "";
		} else {
			additionalMessage = additionalMessage;
		}

		Assert.assertNotNull("List of teachers is null. If no teachers are found, empty list should be returned.",
				results);
		Assert.assertEquals(additionalMessage, firstname, ((Teacher) results.get(recordno)).getFirstName());
		Assert.assertEquals(additionalMessage, lastname, ((Teacher) results.get(recordno)).getLastName());
		if (id != null) {
			Assert.assertTrue(id == (long) ((Teacher) results.get(recordno)).getId());
		}

	}

	private void checkTeacher(Teacher result, String firstname, String lastname, Long id) {
		this.checkTeacher(result, firstname, lastname, id, (String) null);
	}

	private void checkTeacher(Teacher result, String firstname, String lastname, Long id, String additionalMessage) {
		if (additionalMessage == null) {
			additionalMessage = "";
		} else {
			additionalMessage = additionalMessage;
		}

		Assert.assertEquals(additionalMessage, firstname, result.getFirstName());
		Assert.assertEquals(additionalMessage, lastname, result.getLastName());
		if (id != null) {
			Assert.assertTrue(id == (long) result.getId());
		}

	}
}