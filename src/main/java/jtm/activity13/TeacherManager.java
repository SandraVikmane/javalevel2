package jtm.activity13;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TeacherManager {

	protected Connection conn = null;

	public static void main(String args[]) throws SQLException {
		//TeacherManager teacherManager = new TeacherManager();
		// teacherManager.deleteTeacher(1);
		//System.out.println(teacherManager.findTeacher(1));
		// System.out.println(teacherManager.findTeacher("Mara", "Dua"));
		// teacherManager.insertTeacher("Para", "Fyre");
		// System.out.println(teacherManager.ps.executeQuery());
	}

	public TeacherManager() throws SQLException {
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// url =
		// "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		// "jdbc:mysql://127.0.0.1/testjava",
		// user = "student" // "root"
		// pass = "Student007" // ""
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods

		try {
			conn = null;
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "");
			conn.setAutoCommit(false);
			System.out.println("Connection Established Successfull and the DATABASE NAME IS:"
					+ conn.getMetaData().getDatabaseProductName());

		} catch (Exception e) {
			System.out.println("Unable to make connection with DB");
			System.out.println(e);
		}
	}

	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 * @throws SQLException
	 */
	public Teacher findTeacher(int id) throws SQLException {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.teacher"
		Teacher aTeacher = new Teacher(0, null, null);

		try {
			String sqlFindById = "SELECT * FROM database_activity.teacher WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sqlFindById);
			ps.setInt(1, id);
			ResultSet resultSet = ps.executeQuery();
			if (resultSet.next()) {
				//conn.commit();
				aTeacher = new Teacher(id, resultSet.getNString(2), resultSet.getNString(3));
			return aTeacher;				
			} else
				return aTeacher;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aTeacher;
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 * @throws SQLException
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) throws SQLException {

		List<Teacher> teacherList = new ArrayList<Teacher>();

		try {
			String sqlFindByNames = "SELECT * FROM database_activity.teacher WHERE firstname LIKE ? AND lastname LIKE ?";
			PreparedStatement ps = conn.prepareStatement(sqlFindByNames);
			ps.setString(1, "%" + firstName + "%");
			ps.setString(2, "%" + lastName + "%");
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				//conn.commit();
				teacherList.add(new Teacher(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
				//return teacherList;
			}
			// TODO #3 Write an sql statement that searches teacher by first and
			// last name and returns results as ArrayList<Teacher>.
			// Note that search results of partial match
			// in form ...like '%value%'... should be returned
			// Note, that if nothing is found return empty list!
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teacherList;
	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 * @throws SQLException
	 */

	public boolean insertTeacher(String firstName, String lastName) throws SQLException {
		// TODO #4 Write an sql statement that inserts teacher in database.
		try {
			String sqlInsert = "INSERT INTO database_activity.teacher (firstname, lastname) VALUES (?, ?)";
			PreparedStatement ps = conn.prepareStatement(sqlInsert);
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			int count = ps.executeUpdate();
			if (count > 0) {
				conn.commit();
				return true;
			} else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		try {

			String sqlStmt = "INSERT INTO database_activity.teacher (id, firstname, lastname) values (?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sqlStmt);
			ps.setInt(1, teacher.getId());
			ps.setString(2, teacher.getFirstName());
			ps.setString(3, teacher.getLastName());
			int count = ps.executeUpdate();
			if (count > 0) {
				conn.commit();
				return true;
			} else
				return false;
			// TODO #3 Write an sql statement that searches teacher by first and
			// last name and returns results as ArrayList<Teacher>.
			// Note that search results of partial match
			// in form ...like '%value%'... should be returned
			// Note, that if nothing is found return empty list!
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 * @throws SQLException
	 */
	public boolean updateTeacher(Teacher teacher) throws SQLException {
		boolean status = false;
		try {
			String sqlUpdate = "UPDATE database_activity.teacher SET firstname = ?, lastname = ? WHERE id=?";
			PreparedStatement ps = conn.prepareStatement(sqlUpdate);
			ps.setString(1, teacher.getFirstName());
			ps.setString(2, teacher.getLastName());
			ps.setInt(3, teacher.getId());
			int count = ps.executeUpdate();
			if (count > 0) {
				conn.commit();
				status = true;
				return status;
			} else
				return status;
			// TODO #6 Write an sql statement that updates teacher information.

		} catch (SQLException e) {
			conn.rollback();
			e.printStackTrace();
			return status;
		}
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher(int id) throws SQLException {
		// TODO #7 Write an sql statement that deletes teacher from database.
		try {
			String sqlDelete = "DELETE FROM database_activity.teacher WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sqlDelete);
			ps.setInt(1, id);
			//ps.executeUpdate();
			int count = ps.executeUpdate();
			if (count > 0) {
			conn.commit();
			return true;
			} else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void closeConnecion() throws SQLException {
		// TODO Close connection to the database server and reset conn object to null
		conn.close();
		conn = null;
	}

}
