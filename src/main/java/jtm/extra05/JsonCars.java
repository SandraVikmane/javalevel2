package jtm.extra05;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONWriter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jsonTest.TestJSON;

public class JsonCars {

	/*- TODO #1
	 * Implement method, which returns list of cars from generated JSON string
	 */
	
	
	
	public List<Car> getCars(String jsonString) {
		
		List<Car> carList = new ArrayList<Car>();
		
		JSONObject firstObject = new JSONObject(jsonString);
		JSONArray carArr = firstObject.getJSONArray("cars");

		for (int i = 0; i < carArr.length(); i++) {
			JSONObject carItem = carArr.getJSONObject(i);
			String model = carItem.getString("model");
			int year = carItem.getInt("year");
			String color = carItem.getString("color");
			float price = carItem.getFloat("price");
			
			Car car = new Car (model, year, color, price);
			carList.add(car);
						
		}

			/*- HINTS:
			 * You will need to use:
			 * - https://stleary.github.io/JSON-java/org/json/JSONObject.html
			 * - https://stleary.github.io/JSON-java/org/json/JSONArray.html
			 * You will need to initialize JSON array from "cars" key in JSON string
			 */
		return carList;

	}

	/*- TODO #2
	 * Implement method, which returns JSON String generated from list of cars
	 */
	public String getJson(List<Car> carList) {
		
		StringWriter stringWritter = new StringWriter();
		JSONWriter jsonWritter = new JSONWriter(stringWritter);
		jsonWritter.object().key("cars").value(carList).endObject();
		return stringWritter.toString();
		
		/*- HINTS:
		 * You will need to use:
		 * - https://docs.oracle.com/javase/8/docs/api/index.html?java/io/StringWriter.html
		 * - http://static.javadoc.io/org.json/json/20180130/index.html?org/json/JSONWriter.html
		 * Remember to add "car" key as a single container for array of car objects in it.
		 */

	}

}