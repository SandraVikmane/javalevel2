package jtm.extra03;

import java.util.Arrays;

public class PracticalNumbers {
	
	String dummy;
	public PracticalNumbers () {
		this.dummy = "dummy";
	}
	
	public static void main (String[]args) {
		PracticalNumbers instance = new PracticalNumbers();
		instance.getPracticalNumbers(10, 20);
	}


		// TODO Read article https://en.wikipedia.org/wiki/Practical_number
		// Implement method, which returns practical numbers in given range
		// including
		public String getPracticalNumbers(int from, int to) {
			int[] a = new int[to - from];
			int indx = 0;
			for (int i = from; i < to + 1; i++) {
				int sum = 0;
				for (int j = 1; j <= (i / 2); j++) {
					if (i % j == 0) {
						if (!(sum < j - 1))
							sum += j;
					}
				}
				if (sum >= i - 1)
					a[indx++] = i;
			}

			int count = 0;
			for (int i = 0; i < a.length; i++) {
				if (a[i] != 0)
					count++;
			}
			int ans[] = new int[count];
			for (int i = 0; i < count; i++) {
				ans[i] = a[i];
			}

			return Arrays.toString(ans);
		}

	}