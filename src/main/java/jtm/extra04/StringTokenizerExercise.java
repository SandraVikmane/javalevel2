package jtm.extra04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/*-
 * 
 * This class represents string tokenizer exercise.
 */

public class StringTokenizerExercise {

	private String text = "Loremum;is;simply;dummy;text.";
	private String delimiter = ";";
	private BufferedReader input;

	public String[] splitString(String text, String delimiter) {

		this.text = text;
		this.delimiter = delimiter;
		String[] list = text.split(delimiter);
		//StringTokenizer sttk = new StringTokenizer(text, delimiter);
		for (String split : list)
		System.out.println(split);

		// TODO # 1 Split passed text by given delimiter and return array with
		// split strings.
		// HINT: Use System.out.println to better understand split method's
		// functionality.

		return list;
	}

	public List<String> tokenizeString(String text, String delimiter) {

		this.text = text;
		this.delimiter = delimiter;
		List<String> list = new ArrayList<>();
		StringTokenizer sttk2 = new StringTokenizer(text, delimiter);
		String tk = null;
		while (sttk2.hasMoreTokens()) {
			tk = sttk2.nextToken();
			list.add(tk);

			// TODO # 2 Tokenize passed text by given delimiter and return list with
			// tokenized strings.

		}
		return list;
	}

	public static void main(String[] args) {

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		try {
			String filepath = in.readLine();
			String delim = in.readLine();

			StringTokenizerExercise sttk3 = new StringTokenizerExercise();
			List<Student> list = sttk3.createFromFile(filepath, delim);
			System.out.println(list.toString());

		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("Error occured!1");
		}
	}

	public List<Student> createFromFile(String filepath, String delimiter) throws IOException {
		File students = new File(
				"C:\\Users\\PCMAN\\eclipse-workspace\\klO55w\\src\\main\\java\\jtm\\extra04.students.txt");
		List<Student> list = new ArrayList<Student>();
		File students2 = new File(
				"C:\\Users\\PCMAN\\eclipse-workspace\\klO55w\\src\\main\\java\\jtm\\extra04.students2.txt");
		List<Student> list2 = new ArrayList<Student>();

		try {
			input = new BufferedReader(new FileReader(students));
			String lineStr = "\n";
			String[] columns = new String[3];
			String[] columns2 = new String[5];

			StringTokenizer line = new StringTokenizer(lineStr, delimiter);

			Data student = new Data();
			
			
		
			//list.add(student);
			
			StringTokenizer line2 = new StringTokenizer(lineStr, delimiter);			
			Data student2 = new Data();

			//list2.add(student2);

		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("Error occured!2");
		}

		// TODO # 3 Implement method which reads data from file and creates
		// Student objects with that data. Each line from file contains data for
		// 1 Student object.
		// Add students to list and return the list. Assume that all passed data
		// and
		// files are correct and in proper form.
		// Advice: Explore StringTokenizer or String split options.

		return list;
	}

	class Data {
		private String name;
		private String surname;
		private int number;
		private int no;

		public void setName(String name) {
			this.name = name;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}

		public void setNumber(int number) {
			this.number = number;
		}

		public void setNo(int no) {
			this.no = no;
		}

		public String getName(String name) {
			return name;
		}

		public String getSurname(String surname) {
			return surname;
		}

		public int getNumber(int number) {
			return number;
		}

		public int getNo(int no) {
			return no;
		}

	}

	private void setValue(String fieldName, StringTokenizer line, Data student) {
		switch (fieldName) {
		case "No":
			student.setNo((Integer) line.nextElement());
			break;
		case "Name":
			student.setName(String.valueOf((String) line.nextElement()));
			break;
		case "Surname":
			student.setSurname((String) line.nextElement());
			break;
		case "Number":
			student.setNumber(Integer.valueOf((String) line.nextElement()));
			break;
		}
	}

}
