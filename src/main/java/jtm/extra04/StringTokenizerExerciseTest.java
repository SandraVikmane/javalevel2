
package jtm.extra04;

import static jtm.TestUtils.handleErrorAndFail;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.AssertionFailedError;

public class StringTokenizerExerciseTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	private static Logger logger = Logger.getLogger(StringTokenizerExerciseTest.class);

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link main.java.jtm.extra04.StringTokenizerExercise#splitString(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public final void testSplitString() {
		StringTokenizerExercise test = new StringTokenizerExercise();
		String testText = "I;want;to;split;this";
		String delimiter = ";";
		try {
			String[] list = test.splitString(testText, delimiter);
			assertEquals("String length error.", 5, list.length);
			assertEquals("String content error.", "to", list[2]);

			testText = "I,to,split,this";
			delimiter = ",";
			list = test.splitString(testText, delimiter);
			assertEquals("String length error.", 4, list.length);
			assertEquals("String content error.", "split", list[2]);
			logger.info("OK");
		} catch (AssertionFailedError | NullPointerException e) {
			handleErrorAndFail(e);
		}
	}

	/**
	 * Test method for
	 * {@link main.java.jtm.extra04.StringTokenizerExercise#tokenizeString(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public final void testTokenizeString() {
		StringTokenizerExercise test = new StringTokenizerExercise();
		String testText = "I)want)to)split)this)text)in)pieces";
		String delimiter = ")";

		try {
			List<String> list = test.tokenizeString(testText, delimiter);
			assertEquals("List size error.", 8, list.size());
			assertEquals("Content error.", "text", list.get(5));

			testText = "I,want,to";
			delimiter = ",";

			list = test.tokenizeString(testText, delimiter);
			assertEquals("List size error.", 3, list.size());
			assertEquals("Content error.", "want", list.get(1));
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

	/**
	 * Test method for
	 * {@link main.java.jtm.extra04.StringTokenizerExercise#createFromFile(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public final void testCreateFromFile() {
		StringTokenizerExercise test = new StringTokenizerExercise();
		String filePath = "./src/main/java/jtm/extra04/students.txt";
		String delimiter = ";";
		String filePath2 = "./src/main/java/jtm/extra04/students2.txt";
		String delimiter2 = ".";
		try {
			List<Student> list = test.createFromFile(filePath, delimiter);
			assertEquals("List size error.", 3, list.size());
			assertEquals("Content error.", "Wick", list.get(2).getLastName());

			List<Student> list2 = test.createFromFile(filePath2, delimiter2);
			assertEquals("List size error.", 5, list2.size());
			assertEquals("Content error", 26351234, list2.get(3).getPhoneNumber());
			logger.info("OK");
		} catch (Exception e) {
			handleErrorAndFail(e);
		}
	}

}
