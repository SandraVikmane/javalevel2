package jtm.extra06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {

	/**
	 * This method finds out if we can make lucky number from numbers in input
	 * string. Lucky number is number with digit sum equal to 25
	 * 
	 * @param string , needed to be checked
	 * @return true if numbers in this number are lucky, false if not.
	 */
	public boolean isLuckyNumber(String input) {
		String luckyNumber = new String(input);
		String regexNum = "[0-9]+";
		String regexNum2 = "[^0-9]+";
		int number;
		int sum = 0;

		if (luckyNumber.matches(regexNum)) {
			for (int i = 0; i < luckyNumber.length(); i++) {
				number = luckyNumber.charAt(i);
				sum = sum + number;
			}
		} else {
			luckyNumber = luckyNumber.replaceAll(regexNum2, "");
			for (int i = 0; i < luckyNumber.length(); i++) {
				number = luckyNumber.charAt(i);
				sum = sum + number;
			}
		}
		if (sum == 25)
			return true;
		else
			// TODO #1 Remove all non digits from the input.
			// HINT: use negation pattern.

			// TODO #2 count the sum of all digits, and check if the sum is lucky
			return false;
	}

	/**
	 * This method finds Kenny or Kelly hiding in this list. "Kenny" or "Kelly" can
	 * be written with arbitrary number of "n"s or "l"s starting with two.
	 * 
	 * @param input — input string
	 * @return — position of "Kenny" string starting with zero. If there are no
	 *         "Ken..ny" return -1.
	 */
	public int findKenny(String input) {

		String[] kennyKelly = new String[] { input };
		String element;
		String element2;
		String regexElement = "[K][e][n][n]+[y]";
		int position;
		List<String> kennyKellyList = new ArrayList<String>();
		for (int i = 0; i < kennyKelly.length; i++) {
			element = kennyKelly[i];
			kennyKellyList.add(element);
		}
		for (int i = 0; i < kennyKellyList.size(); i++) {
			element2 = kennyKellyList.get(i);
			if (element2.matches(regexElement)) {
				position = Collections.binarySearch(kennyKellyList, element2);
				return position;
			}
		}
		return -1;
	}
	// for (int i = 0; i < kennyKellyList.size(); i++) {
	// position = kennyKellyList.indexOf(element.matches(regexElement));
	// return position;
	// else
	// return -1;

	/**
	 * THis method checks if input string is correct telephone number. Correct Riga
	 * phone number starts with 67 or 66 and is followed by 6 other digits. not
	 * obligate prefix +371
	 * 
	 * @param telephoneNumber - number, needed to be checked.
	 * @return true if number is valid Riga city number.
	 */
	public boolean isGood(String telephoneNumber) {
		String phone = new String(telephoneNumber);
		String regex = "(\\+371)?(66|67)[0-9]{6}";
		if (phone.matches(regex))
			return true;
		else
			// TODO #5 check with "matches" method if this number is valid.
			return false;
	}

	public static void main(String[] args) {
		String regexNum = "[0-9]+";
		String regexDig = "\\d+";
		String getRidOf = "[^a-z]";
		String regex = "(\\+371)?(66|67)[0-9]{6}";
		String luckyNumber = "34567";
		String number = "67876345";
		// System.out.println(luckyNumber.matches(regexNum));
		// System.out.println(luckyNumber.matches(regexDig));
		// System.out.println(luckyNumber.matches(getRidOf));
		// System.out.println(number.matches(regex));

		String lucky = "tg7hfjtn45578";
		String regexNum3 = "[0-9]+";
		String regexNum4 = "[^0-9]+";
		System.out.println(lucky.matches(regexNum3));
		System.out.println(lucky.replaceAll(regexNum4, ""));
		String[] kennyKelly = new String[] { "keny", "kennnny", "kelly" };
		String element;
		List<String> kennyKellyList = new ArrayList<String>();

		for (int i = 0; i < kennyKelly.length; i++) {
			element = kennyKelly[i];
			System.out.println(element);
			kennyKellyList.add(element);
		}
		// System.out.println(kennyKelly.toString());
		System.out.println(kennyKellyList);
		// String regexElement = "[K][e][n][n]+[y]";
		// System.out.println(element.matches(regexElement));
	}
}
