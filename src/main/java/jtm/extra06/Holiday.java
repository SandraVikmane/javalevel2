package jtm.extra06;


/**
 * This enum represents holidays, displayed as month + day value. This enum can
 * give nearest holiday.
 */
public enum Holiday {
	NEW_YEAR(1, 1), WOMAN_DAY(3, 8), CHUCK_NORRIS_BIRTHSDAY(3, 10), FOOLS_DAY9(4, 1), WORLD_END(12, 21);

	int month;
	int day;

	Holiday(int month, int day) {
		// TODO #1 implement class variables for month and day of the holiday
		this.month = month;
		this.day = day;
	}

	public static Holiday getNearest(int currentMonth, int currentDay) {
		Holiday returnHoliday = Holiday.NEW_YEAR;
		for (Holiday holiday : Holiday.values()) {
			if (holiday.getMonth() > currentMonth) {
				return holiday;
			} else if (holiday.getMonth() == currentMonth && holiday.getDay() > currentDay) {
				return holiday;
			}
		}
		return returnHoliday;
	}

	// TODO #2 implement method which will return the nearest holiday.
	// HINT: note, that holidays is arranged by date ascending, so if there
	// are
	// no more holidays this year, first holiday in the list will be the
	// next.


	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public static void main(String[] args) {

		String array[] = { "A", "B", "C", "D" };
		for (String s : array) {
			if (s == "B")
				continue;

			System.out.println(s);
		}

	}
}
