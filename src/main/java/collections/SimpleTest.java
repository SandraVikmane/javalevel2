package collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javassist.bytecode.Descriptor.Iterator;

public class SimpleTest {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		List<Object> list = new ArrayList<Object>();
		list.add(4324);
		list.add("ttt"); //norādot tipu "Object" listei var pievienot jebko.
		list.add(new SimpleTest());
		System.out.println(list);

		Set<Integer> set = new TreeSet<Integer>();
		set.add(5);
		set.add(7);
		set.add(5);
		set.add(44);

		System.out.println(set.toString());

		Queue<String> queue = new PriorityQueue<String>();
		queue.add("Janis");
		queue.add("Peteris");
		queue.add("Davis");
		// queue.poll(); // remove the element which is the next in the queue
		// queue.remove(); // remove the last element in the queue

		// Iterator iterator = queue.iterator();
		// while (iterator.hasNext())
		// System.out.println(iterator.next());

		System.out.println(queue.toString()); // izprinte sakot ar pedejo
		System.out.println(queue.element()); // izprinte pedejo elementu
		System.out.println(queue.peek()); // izprinte pedejo elementu

		Map map = new TreeMap<Integer, String>();

		map.put(1121, "Janis");
		map.put(1321, "Peteris");
		// map.put(1321, "Oskars"); the keys must be unique, if the key the same, then
		// the value is overwritten
		System.out.println(map.toString()); // treemap gets sorted automatically by keys
		// map.put(1121, "Janis");

		System.out.println(map.get(1321));
		// System.out.println(map.remove(1321)); // deletes element form the map. 1st
		// read the entry, then 2. delete the entry 3. aligne the sequence of the
		// entries (if sorted array)
		System.out.println(map.containsKey(1521));
		System.out.println(map.containsValue("Signe"));
		// System.out.println(map.isEmpty());

		Map map2 = new TreeMap<Integer, String>();
		map2.put(2456, "Jekabs");
		map.putAll(map2);
		System.out.println(map2.toString());

		Map<Integer, TableClassTest> mapTable = new TreeMap<Integer, TableClassTest>();
		mapTable.put(1, new TableClassTest(723, "Im the value", true));
		mapTable.put(1, new TableClassTest(344567, "Im the second value", false));
		System.out.println(mapTable.toString());

		Map<CostumKey, TableClassTest> mapTable2 = new TreeMap<CostumKey, TableClassTest>();
		CostumKey key1Obj = new CostumKey();
		key1Obj.key1 = "KeyString1";
		key1Obj.key2 = 45673;
		
		CostumKey key2Obj = new CostumKey();
		key2Obj.key1 = "KeyString1";
		key2Obj.key2 = 45672; 
		
		mapTable2.put(key1Obj, new TableClassTest(723, "Im the value", true));
		mapTable2.put(key2Obj, new TableClassTest(344567, "Im the second value", false));
		
		System.out.println(mapTable2.toString());
	}

}

class TableClassTest {
	public int value1;
	public String value2;
	public boolean value3;

	public TableClassTest(int value1, String value2, boolean value3) {
		// TODO Auto-generated constructor stub
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "value1 = " + this.value1 + ", value2 = " + this.value2 + ", value 3 = " + this.value3;
	}
}

class CostumKey implements Comparable<CostumKey> {

	/*
	 * CustomKey obj1 = new..; CustomKey obj2 = new..;
	 * 
	 * obj1.compareTo(obj2); => obj1 = obj2
	 */

	public String key1;
	public int key2;

	public int compareTo(CostumKey o) {

//		return 0;

		return this.getKey().compareTo(o.getKey());
	};

	@Override
	public String toString() {
		return "key1 = " + this.key1 + "; key2 = " + this.key2;
	}

	public String getKey() {
		return this.key1 + this.key2;
	}
}

class Employee implements Comparable<Employee> {

	public int skillLevel;
	public int age;

	@Override
	public int compareTo(Employee o) {

		int total1 = this.skillLevel * 50 - this.age * 10;
		int total2 = o.skillLevel * 50 - this.age * 10;

		if (total1 > total2)
			return 1;
		else if (total1 < total2)
			return -1;
		else
			return 0;
	}

}
