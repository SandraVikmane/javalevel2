package collections;

public class CustomKey implements Comparable<CustomKey> {

	/*
	 * CustomKey obj1 = new..; CustomKey obj2 = new..;
	 * 
	 * obj1.compareTo(obj2); => obj1 = obj2
	 */

	public String key1;
	public int key2;

	public int compareTo(CustomKey o) {

//		return 0;

		return this.getKey().compareTo(o.getKey());
	};

	@Override
	public String toString() {
		return "key1 = " + this.key1 + "; key2 = " + this.key2;
	}

	public String getKey() {
		return this.key1 + this.key2;
	}
}

class Employee implements Comparable<Employee> {

	public int skillLevel;
	public int age;

	@Override
	public int compareTo(Employee o) {

		int total1 = this.skillLevel * 50 - this.age * 10;
		int total2 = o.skillLevel * 50 - this.age * 10;

		if (total1 > total2)
			return 1;
		else if (total1 < total2)
			return -1;
		else
			return 0;
	}



	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
