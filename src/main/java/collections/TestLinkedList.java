package collections;

import java.util.LinkedList;
import java.util.ListIterator;

public class TestLinkedList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<String> list = new LinkedList<String>();

		list.add("Element1");
		list.add("Element2");
		list.add("Element3");
		list.add("Element4");
//		System.out.println(list);
//		System.out.println(list.lastIndexOf("Element2"));

		ListIterator<String> iter = list.listIterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

	}

}
