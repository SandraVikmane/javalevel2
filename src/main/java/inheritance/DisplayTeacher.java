package inheritance;

public class DisplayTeacher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Teacher teacher = new Teacher();
		teacher.does();

		teacher = new MathTeacher();
		teacher.does();
		MathTeacher mathteacher = (MathTeacher) teacher;

		teacher = new ChemistryTeacher();
		teacher.does();

		try {
			mathteacher = (MathTeacher) teacher;
		} catch (ClassCastException e) {
			// TODO: handle exception
			System.out.println("Casting from " + teacher.getClass().getSimpleName() + " to MathTeacher is not possible");
		}
		
		if(teacher instanceof MathTeacher) System.out.println("Teacher is Math teacher");
		else if(teacher instanceof ChemistryTeacher) System.out.println("Teacher is Chemistry teacher");
		

	}

}

class Teacher {
	private String designation = "Teacher";
	private String collegeName = "Science College";
	protected String mainSubject;

	public void does() {
		System.out.println("Teaching");
	}

}

class ChemistryTeacher extends Teacher {

	public ChemistryTeacher() {
		// TODO Auto-generated constructor stub
		this.mainSubject = "Chemistry";
	}

	@Override
	public void does() {
		// TODO Auto-generated method stub
		System.out.println("Teaching Chemistry");
	}
}

class MathTeacher extends Teacher {

	public MathTeacher() {
		// TODO Auto-generated constructor stub
		this.mainSubject = "Mathematics";
	}

	@Override
	public void does() {
		// TODO Auto-generated method stub
		System.out.println("Teaching Math");
	}

}
