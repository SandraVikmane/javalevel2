package inheritance;

class SuperSuperEx{

	protected SuperSuperEx() {
		// TODO Auto-generated constructor stub
		System.out.println("Super Super class is called");
	}
}

class SuperClassEx extends SuperSuperEx {

	protected SuperClassEx(int i) {
		// TODO Auto-generated constructor stub
		System.out.println("Super class is called");
	}

}

public class ConstructorsExample extends SuperClassEx {

	protected ConstructorsExample(int i) {
		super(i);
		System.out.println("Sub class is called");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ConstructorsExample instance = new ConstructorsExample(0);
	}

}

class SubContrstructors extends ConstructorsExample {

	protected SubContrstructors(int i) {
		super(i);
		// TODO Auto-generated constructor stub
	}

}
