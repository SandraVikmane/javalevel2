package inheritance;

public class Person {

	protected enum Gender {
		MALE, FEMALE, UNDEFINED
	}

	protected String name;
	protected Date birthDate;
	protected Gender gender;

	protected Person() {
		// TODO Auto-generated constructor stub
	}

	public void describe() {
		/// Nothing to implement
	}

	public String getDetails() {

		return "The Person " + this.name + ", is born on " + this.birthDate;
	}

}

class Employee extends Person {
	protected double salary;
	protected int experince;
	protected Position position;

	protected String ref = "_";
	protected String ref2 = "_";

	public Employee() {
		// TODO Auto-generated constructor stub
		this.ref = "_";
		this.ref2 = "_";

		switch (gender) {
		case MALE:
			this.ref = "his";
			this.ref2 = "he";
			break;
		case FEMALE:
			this.ref = "her";
			this.ref2 = "she";
			break;
		}
	}

	@Override
	public String getDetails() {
		String part1 = super.getDetails();
		String part2 = ", " + this.ref + "has " + this.experince + " years of experience";
		return part1 + part2;
	}

	@Override
	public void describe() {
		// TODO Auto-generated method stub
		System.out.println("Here is the description of the employee!");
	}

	protected void setPosition(String position) {
		switch (position) {
		case "J Dev.":
			this.position = Position.JUNIOR_DEVELOPER;
			break;
		case "Manager":
			this.position = Position.MANAGER;
			break;
		case "Senior Dev.":
			this.position = Position.SENIOR_DEVELOPER;
			break;
		}

	}

}

class Manager extends Employee {
	protected String department;

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		String part1 = super.getDetails();
		String part2 = ", " + this.ref2 + " works in " + this.department + " depratment";
		return part1 + part2;
	}

}

class Date {
	private int month;
	private int year;
	private int day;

	public void setMonth(int a) {
		if (a > 0 && a < 13)
			this.month = a;
		else
			System.out.println("Month is entered incorreclty!");

	}

	public int getMonth() {
		return this.month;
	}

	public void setYear(int a) {
		if (a < 2021)
			this.year = a;
		else
			System.out.println("Year is entered incorreclty!");

	}

	public int getYear() {
		return this.year;
	}

	public void setDay(int a) {
		if (a > 0 && a < 32)
			this.day = a;
		else
			System.out.println("Day is entered incorreclty!");

	}

	public int getDay() {
		return this.day;
	}

}
