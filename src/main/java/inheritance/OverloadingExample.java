package inheritance;

public class OverloadingExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		OverloadingExample instance = new OverloadingExample();
	}

	private int sum(int a, int b) {
		return a + b;
	}

	private int sum(int a, int b, int c) {
		return this.sum(a, b) + c;
	}

}
