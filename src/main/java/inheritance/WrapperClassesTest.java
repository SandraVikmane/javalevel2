package inheritance;

import java.lang.Integer;
import java.util.ArrayList;

public class WrapperClassesTest {

	private static ArrayList<Integer> myNumbers = new ArrayList<Integer>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Integer val = 7;
		Integer val2 = 3;
		String val_string = val.toString();
		
		int val_int = 7;
		val_string = Integer.toString(val_int);

		System.out.println();

		myNumbers.add(3);
		myNumbers.add(8);

	}

}
