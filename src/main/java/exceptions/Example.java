package exceptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Example {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PictureBuilder picBuilder = new PictureBuilder();
		picBuilder.run();
	}

}

class PictureBuilder {
	private int z, n;
	private String[] input, lines;
	private String lineBreak = "";

	public void run() {
		try {
			this.read();
			this.checkValidation();
			this.draw();
		} catch (IOException e) {
			System.out.println("Input/Output exception occured");
		}
		catch (IncorrectInoutDate e) {
			e.printStackTrace();
		}
		
	}
	
	private void read() throws IOException{
		BufferedReader read = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter Z: ");
		this.z = Integer.parseInt(read.readLine());
		System.out.println("Enter N: ");
		this.n = Integer.parseInt(read.readLine());
		this.lines = new String [this.z];
	}
	
	private void checkValidation () throws IncorrectInoutDate {
		if (!(this.z > 0 && this.z < 20) || !(this.n > 0 && this.n < 30)) 
			throw new IncorrectInoutDate(this.z, this.n);
		
	}
	
	public void draw() {
		for (int i = 0; i < this.z; i++) {
			this.lineBreak += " ";
		}
		int currentNumber = this.z;
		for (int i = 0; i < this.z; i++) {
			this.lines[i] = this.lineBreak;
			for (int j = 0; j < this.z; j++) {
				this.lines[i] += "+ ";
			}
			currentNumber -= 1;
			this.lineBreak += " ";
		}
		for (int i = this.z-1; i >=0; i--) {
			System.out.println(lines[i]);
			
		}
		
	}
}

class IncorrectInoutDate extends Exception {
	int z, n;
	
	public IncorrectInoutDate(int z, int n) {
		// TODO Auto-generated constructor stub
		this.z = z;
		this.n = n;
	}
	
	@Override
	public void printStackTrace() {
		// TODO Auto-generated method stub
		System.out.println("input z = " + this.z + ", n = " + this.n + "is incorrect!");
	}
	
}