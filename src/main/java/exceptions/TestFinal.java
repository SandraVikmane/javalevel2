package exceptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TestFinal {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		try {
			FileInputStream file = new FileInputStream(".src/file.txt");
			file.close();
		} finally {
			System.out.println("Finally is called");
		}
		System.out.println("After finally!");

	}

}

class ExA {

	void void1() throws TestExceptions, TestExceptions2 {

		if (1 == 1)
			throw new TestExceptions();
		else
			throw new TestExceptions2();
	}

	void void2() throws TestExceptions, TestExceptions2 {
		this.void1();
	}

	void void3() {

		try {
			this.void2();
		} catch (Exception e) {
		}
	}

}

class TestExceptions extends Exception {
}

class TestExceptions2 extends Exception {
}
