package exceptions;

public class Simple {

	public static void main(String[] args) {

		int c;
		int a = 2;
		int b = 0;

		try {
//			if (1 == 1) {
//				throw new ClassCastException();
//			}

			//A object = A.getInstance();
			//object.test();
			c = Simple.divide(a, b);
			c = 2;
			assert c == 1 : "Not correct";

		} catch (ArithmeticException e) {
			System.out.println("Cannot divide by zero!!");
			//b += 1;
			c = Simple.divide(a, b);
			System.out.println(c);
		

//		} catch (NullPointerException e) {
//			// TODO: handle exception
//			System.out.println("You can't access the object which is not initialized");
		}

//		catch (Error e) {
//			// TODO: handle exception
//			System.out.println("I've thrown the exception");
//		}

	}

	private static int divide(int a, int b) {
		return a / b;
	}

}

class A {

	static A getInstance() {
		return null;
	}

	public void test() {

	}

}
