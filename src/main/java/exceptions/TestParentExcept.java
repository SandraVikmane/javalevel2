package exceptions;

public class TestParentExcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A2 obj = new A2();

	}

}

class A1 { // parent class
	public void method() throws Excep1 { // public method which throws the exception

	}

}

class A2 extends A1 {
	@Override
	public void method() throws Excep1 { // exception can be form the super class or child class.
		// TODO Auto-generated method stub
	}

}

class Excep1 extends Exception {

}

class Excep2 extends Excep1 {

}