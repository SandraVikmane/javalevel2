package exceptions;

public class ClassNotFound {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Class.forName("DummyClass");
		} catch (Exception e) {
			e.printStackTrace();
		}

	} // exception "class not found", because we are looking for a class that does not exists.
// usually there is problem with a built path, if this exception is visible.
}
