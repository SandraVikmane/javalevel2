package exceptions;

import java.util.ArrayList;
import java.util.List;

public class TestVoting {

	public static void main(String[] args) {

		Person me = new Person(22);
		me.setDebt(0);
		me.setVotingPlace("The second grammar school");

		Voting voting = new Voting(me);
		boolean voted = false;

		do {

			try {
				voting.vote("Any canditade");
				voted = true;
				System.out.println("Thanks for the vote!");
			} catch (InvalidAgeException e) {
				e.printStackTrace();
				return;
			} catch (DebtNotPayed e) {
				e.printStackTrace();
				int toPay = e.getToPay();
				me.payDebt(toPay);
			} catch (WrongPlaceException e) {
//				StackTraceElement element[] = e.getStackTrace();
	//			System.out.println(element[0].getMethodName());
				//System.out.println(e.getLocalizedMessage());
				e.printStackTrace();
				return;
			}
		} while (voted == false);

	}

}

class Person {

	private int age;
	private int debt = 0;
	private String votingPlace;

	public Person(int age) {
		this.age = age;
	}

	public void setVotingPlace(String place) {
		this.votingPlace = place;
	}

	public String getVotingPlace() {
		return this.votingPlace;
	}

	public void setDebt(int amount) {
		this.debt = amount;
	}

	public int getDebt() {
		return this.debt;
	}

	public int getAge() {
		return this.age;
	}

	public void payDebt(int amount) {
		this.debt -= amount;
	}
}

class Voting {

	static private List<String> votes = new ArrayList<String>();
	static private String VOTING_PLACE = "The first grammar school";
	private Person voter;

	public Voting(Person voter) {
		this.voter = voter;
	}

	public void vote(String candidate) throws InvalidAgeException, DebtNotPayed, WrongPlaceException {
		this.checkAllowed();
		Voting.votes.add(candidate);
	}

	private void checkAllowed() throws InvalidAgeException, DebtNotPayed, WrongPlaceException {

		int age = this.voter.getAge();
		int debt = this.voter.getDebt();

		if (this.voter.getVotingPlace() != VOTING_PLACE)
			throw new WrongPlaceException("The voting place " + VOTING_PLACE + " is not correct for you!");
		if (age < 18)
			throw new InvalidAgeException(age);
		if (debt > 0)
			throw new DebtNotPayed(debt);
	}

}

class DebtNotPayed extends Exception {

	private int debt;

	public DebtNotPayed(int debt) {
		this.debt = debt;
	}

	@Override
	public void printStackTrace() {
		super.printStackTrace();
		System.out.println("You need to pay the debt!");
	}

	public int getToPay() {
		return this.debt;
	}

}

class InvalidAgeException extends Exception {

	private int age;

	public InvalidAgeException() {
		super();
	}

	public InvalidAgeException(String s) {
		super(s);
	}

	public InvalidAgeException(int age) {
		this.age = age;
	}

	@Override
	public void printStackTrace() {
		super.printStackTrace();
		System.out.println("Age " + this.age + " is not valid vor voting!");
	}

}

class WrongPlaceException extends Exception {

	public WrongPlaceException() {
		super();
	}

	public WrongPlaceException(String text) {
		super(text);
	}
	
	public void printStackTrace() {
		super.printStackTrace();
		System.out.println(" is not valid vor voting!");
	}

}