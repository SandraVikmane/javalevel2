package additional;

public class TestGenericTypes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Box obj1 = new Box();
		obj1.set(new TV());

		Box obj2 = new Box();
		obj2.set(new Smartphone());

		TypedBox<TV, Smartphone> typedBox = new TypedBox();
		typedBox.setObject1(new TV());
		typedBox.setObject2(new Smartphone());

	}

}

class TypedBox<T1, T2> {
	private T1 object1;
	private T2 object2;

	public void setObject1(T1 object) {
		this.object1 = object;
	}

	public void setObject2(T2 object) {
		this.object2 = object;
	}

	public T1 getObject1() {
		return this.object1;
	}

	public T2 getObject2() {
		return this.object2;
	}

}

class Box {
	private Object object;

	public void set(Object object) {
		this.object = object;
	}

	public Object get() {
		return object;
	}

}

class TV {
}

class Smartphone {
}
