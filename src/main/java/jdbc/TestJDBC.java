package jdbc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.*;

public class TestJDBC {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// enstablish the connection. the connection is an object
		Connection con = null; // start with null because use try-catch, + final

		
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver"); // we load the driver, provide the name of the class
			
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/testjava", "root", "");
			con.setAutoCommit(false);
			Statement stmt = con.createStatement(); // in statements we write the mysql query

			BufferedReader reader = new BufferedReader (new InputStreamReader(System.in));
			
			//String id = reader.readLine();
			String name = reader.readLine();
			//String surname = reader.readLine();
			//String city = reader.readLine();
			
			Date date = new Date(87878787);
			
			//stmt.execute(
					//"INSERT INTO `test_table`(`ID`, `NAME`, `SURNAME`, `CITY`, `DATE`) VALUES (7, \"Janis\",\"Kalnins\",\"Riga\",\"20100423\")");
			//ResultSet rs = stmt.executeQuery("SELECT * FROM `test_table`");

			int count = stmt.executeUpdate("DELETE FROM `test_table` WHERE `NAME` = " + name + ";");
			
			String sql = "update test_table set NAME=?, SURNAME=? where ID=?";
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, "Peteris");
			preparedStatement.setString(2, "Liepins");
			preparedStatement.setInt(3, 1);
			System.out.println(preparedStatement.executeUpdate());
			con.commit();
			con.close();
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/testjava", "root", "");
			stmt = con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM `test_table`");
			
			while (rs.next())
				System.out.println(rs.getInt("ID") + "; " + rs.getString("NAME") + "; " + rs.getString("SURNAME") + "; "
						+ rs.getString("CITY") + "; " + rs.getDate("DATE")); // print out all the entry from the data base

			//System.out.println("Count is: " + count);
			
		} catch (Exception e) {
			System.out.println(e);
		} finally { // if we have the connection, we want to close it sometime
			try {
				con.close(); // we need to close the connection do not matter if the error occurred o not
			} catch (Exception e) {
				System.err.println(e);
			}

		}
	}
}
